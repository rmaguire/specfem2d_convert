#!/usr/bin/env python                                                                     
import numpy as np
from numpy.distutils.core import setup
from setuptools import find_packages

setup(name='sem2d_convert',
      version='0.1',
      description='Conversion from',
      author='Ross Maguire',
      author_email='rmaguir@umd.edu',
      packages = find_packages(),
      install_requires = ['obspy'],
      scripts = ['specfem2d_convert/specfem2d_convert.py',
                 'scripts/specfem2d_to_su',
                 'scripts/specfem2d_to_segy'],
      url='www.gitlab.com/rmaguire/specfem2d_convert')
