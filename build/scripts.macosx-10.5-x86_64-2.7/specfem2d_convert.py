import sys
import glob
import obspy
import argparse
import numpy as np
from obspy.core import AttribDict
from obspy import Trace, Stream, UTCDateTime
from obspy.io.segy.segy import SEGYTraceHeader, SEGYBinaryFileHeader

parser = argparse.ArgumentParser()
parser.add_argument('--stations_file',help='specfem2d stations used file')
parser.add_argument('--source_file',help='specfem2d sources used file')
parser.add_argument('--component',help='e.g., "X", or "Z"')
parser.add_argument('--seismogram_dir',help='directory containing specfem2d seismograms')
parser.add_argument('--output_format',help='segy or su')
parser.add_argument('--sampling_rate',help='sampling rate of data (defaults to sampling rate of synthetics')
parser.add_argument('--output_file',help='name of output file')
parser.add_argument('--verbose',action='store_true', help='add this flag for output messages')
args, leftovers = parser.parse_known_args()

def read_stations(stations_file):
    f = np.genfromtxt(stations_file)
    x = f[:,2]
    y = f[:,3]
    return x,y

def read_source(source_file):
    f = np.loadtxt(source_file)
    if f.shape == (2,):
        x = f[0]
        y = f[1]
    else:
        x = f[:,0]
        y = f[:,1]
    return x,y

def read_seismogram_list(seismogram_dir,component):
    seismo_files = glob.glob(seismogram_dir+'/*.??'+component+'.semd')
    seismo_files.sort()
    return seismo_files

def main():
    stations_x, stations_y = read_stations(args.stations_file)
    source_x, source_y = read_source(args.source_file)
    seismo_files = read_seismogram_list(args.seismogram_dir,args.component)

    stream = Stream() 

    stream.stats = AttribDict
    stream.stats.textual_file_header = 'Textual Header'
    stream.stats.binary_file_header = SEGYBinaryFileHeader()
    stream.stats.binary_file_header.trace_sorting_code = 5

    for i,seismo_file in enumerate(seismo_files):
        f = np.loadtxt(seismo_file)
        time = f[:,0]
        data = f[:,1]
        delta = np.diff(time)[0]
        st_x = stations_x[i]
        st_y = stations_y[i]

        trace = Trace(data)
        trace.stats.sampling_rate = 1./delta
        trace.stats.delta = delta

        if args.sampling_rate is not None:
            trace.resample(float(args.sampling_rate))
            delta = 1./float(args.sampling_rate)

            if args.verbose:
                print 'CHANGED SAMPLING RATE TO', args.sampling_rate

        trace.stats.starttime = UTCDateTime(2011,11,11,11,11,11)
        trace.data = np.require(trace.data,np.float32)

  
        if not hasattr(trace.stats, 'segy.trace_header'):
            trace.stats.segy = {}
        trace.stats.segy.trace_header = SEGYTraceHeader()
        trace.stats.segy.trace_header.trace_sequence_number_within_line = i+1
        trace.stats.segy.trace_header.group_coordinate_x = st_x
        trace.stats.segy.trace_header.group_coordinate_y = st_y
        trace.stats.segy.trace_header.source_coordinate_x = source_x
        trace.stats.segy.trace_header.source_coordinate_y = source_y

        stream += trace

    if args.output_format == 'segy':
        stream.write(args.output_file+'.sgy', format='SEGY', data_encoding=1, byteorder=sys.byteorder)#, textual_header_encoding='ASCII')
    elif args.output_format == 'su':
        stream.write(args.output_file+'.su', format='SU')
    else:
        raise ValueError('format not available')

main()
